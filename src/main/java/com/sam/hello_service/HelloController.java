package com.sam.hello_service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sam.hello_service_api.HelloService;
import com.sam.hello_service_api.User;

@RestController
public class HelloController implements HelloService {
	private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(HelloController.class);
	@Autowired
	DiscoveryClient discoveryClient;

	@RequestMapping("/hello")
	public String hello() {
		ServiceInstance instance = discoveryClient.getLocalServiceInstance();
		// 打印服务的服务id
		LOG.info("打印服务的服务id*********" + instance.getServiceId());
		return "hello,this is hello-service";
	}

	@Override
	public String hello2() {
		LOG.info("hello-service项目com.sam.hello_service.HelloController.hello2()");
		return "hello,this is hello2-service";

	}

	@Override
	public User printUser(@RequestBody User user) {
		LOG.info("hello-service项目com.sam.hello_service.HelloController.printUser(@RequestBody User user)");
		return user;
	}
}
