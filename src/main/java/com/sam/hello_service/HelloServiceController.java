package com.sam.hello_service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.serviceregistry.Registration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloServiceController {
	private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(HelloServiceController.class);
	@Autowired
	DiscoveryClient discoveryClient;
	@Autowired
	Registration registration;

	@RequestMapping("/myHello")
	public String myHello() {
		String serviceId = registration.getServiceId();
		ServiceInstance instance = discoveryClient.getLocalServiceInstance();
		// 打印服务的服务id
		LOG.info("HelloServiceController打印服务的服务id*********" + instance.getServiceId()+"; serviceId=="+serviceId);
		return "hello,this is hello-service HelloServiceController.myHello()";
	}

}
